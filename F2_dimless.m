function out = F2_dimless(omega_new)
    over = tanh(2.5 * omega_new^2 - 3.0) + 1;
    under = 4;
    first = over / under;

    second = 0.428 * exp(-20 * (omega_new - 0.9)^2);
    out = first + second;
end
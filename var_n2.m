function [var] = var_n2()
clc
    T_1 = 10;
    c = 1;
    b = 1;
    
    function out = fun(mu)
        over = SF(mu, T_1);
        under = (c - mu*mu)^2 + b*b*mu*mu;
        out = over/under;
    end

    var = integral2(@(x)fun(x), 0, 2, 30, 1);
    
end




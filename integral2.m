function [ sum ] = integrate(fun, xmin, xmax, steps, flag)
%INTEGRATE Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 5
        flag = 0;
    end
    step_size = (xmax - xmin)/steps;
    
    sum = 0;
    ys = [];
    step_list = 1:steps;
    for i = step_list;
        step = i * step_size;
        ys(i) = fun(step)*step_size;
        sum = sum + ys(i);
    end
    
    if flag == 1
        plot(step_list*step_size, ys);
    end

end


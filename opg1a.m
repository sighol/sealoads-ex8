function opg1a()

B_over_D = 2.0;
A_over_BD = 0.95;
  
T = [5 10 15];
H_13 = [2.5 5.0 10.0];
Color = ['r', 'g', 'b'];

omega = [0:0.01:2];
hold on;
for i = 1:3
    T_1 = T(i);
    T_2 = T_1 / 1.086;
    T_0 = T_2 * 1.408;
    fprintf('H_13: %5.2f, T_1: %5.2f, T_2: %7.3f T_0: %7.3f\n', H_13(i), T_1, T_2, T_0);
    y = zeros(length(omega), 1);
    for j = 1:length(omega)
        y(j) = H_13(i)*H_13(i) * T(i) *S(T(i), omega(j));
    end
    subplot(3,1,i);
    plot(omega, y, Color(i));
end

legend('H=2.5', 'H=5', 'H=10');

end
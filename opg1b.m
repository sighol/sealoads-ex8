% omega_new = omega * sqrt(D/g)

function  opg1b(omega_new)

    close all;

    fig = figure();
    omega_new = [0:0.01:2];
    y = zeros(length(omega_new), 1);
    for i = 1:length(omega_new)
        y(i) = F2_dimless(omega_new(i));
    end
    plot(omega_new, y)
    title('F_2 enhetslos')
    xlabel('\omega * sqrt(D/g)');
    ylabel('F_2 / (p g \zeta^2)')
    saveas(fig, 'opg1b.png');



end
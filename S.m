function out =  S(omega, T_1)

    u = omega * T_1 / (2 * pi);
    out = (0.11/(2*pi)) * u^-5 * exp (-0.44 * u^-4);
end
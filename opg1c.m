function [ y ] = opg1c()
close all;
D = 10; % meter
H_13 = 5; % meter
T_1 = 10; % sekunder

a = 0;
b = 4;
step = (b-a)/100;

my = (a+step):step:b;
len = length(my);
y = zeros(len, 1);
for i = 1:len
    y(i) = SF(my(i), T_1)*step;
end
    
plot(my, y);
xlabel('\mu');
ylabel('S_x(\mu)');

end


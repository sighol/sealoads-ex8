function [ out, y ] = SF(my, T_1)

rho = 1024;
g = 9.81;
% 
% a = 0;
% b = 100;
% 
% step = (b-a)/1e4;
% 
% x = (a+step):step:b;
% y = zeros(length(x), 1);
% for i = 1:length(x)
%     in = inner(x(i));
%     y(i) = step * in;
% end
% 
% out = 8 * sum(y);
% 
fun = @(x) inner(x);
out = 8 * integral2(fun, 0.01, 100, 10000, 1);

    function out = inner(omega)
        first = S(omega, T_1) * S(omega + my, T_1);
        
        frac = F2_dimless(omega + my/2) * rho * g;
        second = frac^2;
        out = first * second;
    end

end


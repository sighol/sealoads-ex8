function opg1d()
close all;
D = 10;
B = 2*D;
A_22_2d = 1.5* pi * 1025* (D)^2;
A_22 = A_22_2d/2;

m = A_22 + 0.95*B*D*1025


c = m / (100*100);
my_n = 2*pi/100;

b = 0.05 *2* m * my_n
in = struct;
in.c = c;
in.b = b;
in.m = m;
in.A_22 = A_22;

sima_sq = sigma_n2_squared(in);
sigma = sqrt(sima_sq)


end

function res = sigma_n2_squared(in)

    fun = @(x) sigma_n2_squared_inner(x, in);
    res = integral2(fun, 0.01, 0.03, 30, 1);

end

function out = sigma_n2_squared_inner(my, in)
    over = SF(my, 10);
    under = (in.c - in.m * my*my)^2 + in.b^2 * my^2;
    out = over / under;
end